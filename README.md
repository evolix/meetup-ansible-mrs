# Meetup Ansible à Marseille

Dépôt public lié au meetup organisé à Marseille par Evolix.

## Infos générales

Le meetup est organisé régulièrement – tous les 3 ou 4 mois. Les informations et inscriptions se font via le site Meetup.com sur la page [Ansible-Marseille](https://www.meetup.com/fr-FR/Ansible-Marseille/)

## Présentations

* **20 décembre 2018** : « Présentation générale d'Ansible, nouvelles du projet et histoire d'un bug » par Grégory Colpart et Jérémy Lecour.

* **28 mars 2019** : « L'actualité d'Ansible » par Grégory Colpart, et « Trucs & Astuces, issus de cas réels » par Jérémy Lecour.
